#!/bin/bash

################################################################################
# USAGE:
# 
# $ md2pdf.sh [<markdown-file> <latex-template>]
#
# Convert bunch of Markdown files to PDF using Pandoc.
# 
# Both <markdown-file> and <latex-template> must be supplied as positional
# arguments in that order.
#
# Otherwise, if not arguments are supplied, md2pdf.sh grabs
#   * all the Markdown files from a directory `RESOURCE_DIR`,
#   * a Latex template `TEMPLATE` and metadata file `METADATA`
#     from a directory `TEMPLATE_DIR`
# all under the current working directory.
################################################################################

TEMP="/tmp/lecture-notes.md"
OUTPUT="$(pwd)/lecture-notes.pdf"
TEMPLATE_DIR="$(pwd)/templates/"
RESOURCE_DIR="$(pwd)/notes/"

TEMPLATE="$TEMPLATE_DIR/lecture.tex"
METADATA="$TEMPLATE_DIR/metadata.yaml"
RESOURCE="$RESOURCE_DIR/*.md"

DATE_START=$(stat -c %y "$OUTPUT")

# if ARG1 and ARG2 are supplied...
if [[ "$1" != "" && "$2" != "" ]]; then
    cat $1 > $TEMP
    TEMPLATE="$(pwd)/$2"
else
    # check resource dir exists in current directory.
    if [[ ! -d "$RESOURCE_DIR" ]]; then
        echo "Directory '$(basename "${RESOURCE_DIR}")' not found in current directory."
        exit
    fi

    # check template dir exists in current directory.
    if [[ ! -d "$TEMPLATE_DIR" ]]; then
        echo "Directory '$(basename "${TEMPLATE_DIR}")' not found in current directory."
        exit
    fi

    # check pandoc template exists.
    if [[ ! -f "$TEMPLATE" ]]; then
        echo "File '$(basename "${TEMPLATE}")' not found in current directory."
        exit
    fi

    # check pandoc template exists.
    if [[ ! -f "$METADATA" ]]; then
        echo "File '$(basename "${METADATA}")' not found."
        echo "Continuing without it..."
    fi

    # create a temp. file with all MDs from resource files concatenated.
    echo > $TEMP
    for f in "$RESOURCE"; do
        cat $f >> $TEMP
    done
fi

echo "Converting Markdown to PDF..."

pandoc "$TEMP"                  \
--from="markdown"               \
--pdf-engine=pdflatex           \
--resource-path="$RESOURCE_DIR" \
--template="$TEMPLATE"          \
--metadata-file="$METADATA"     \
--filter="pandoc-crossref"      \
--to="pdf"                      \
-o "$OUTPUT"

DATE_END=$(stat -c %y "$OUTPUT")

if [[ $DATE_START != $DATE_END ]]; then
    echo "File '$(basename $OUTPUT)' created successfully."
fi
