# :books: Operating Systems Notes

[**Download PDF!**](https://gitlab.com/uzluisf/hunter-cs340-notes/-/raw/master/lecture-notes.pdf?inline=false)

## Table of Contents

|Operating System Concepts|
|:--------------------------------------|
|[01: Introduction to Operating Systems](./notes/01-introduction-os.md)|
|[02: Operating System Structures](./notes/02-operating-system-structures.md)|
|[03: Processes](./notes/03-processes.md)|
|[04: Threads](./notes/04-threads.md)|
|[05: CPU Scheduling](./notes/05-cpu-scheduling.md)|
|[06: Synchronization](./notes/06-synchronization.md)|
|[07: Synchronization Problems](./notes/07-synchronization-problems.md)|
|[08: Deadlock](./notes/08-deadlock.md)|

**Note:** This isn't a verbatim "translation". For example, all the activities
were removed. There are other minor changes throughout the text as well.

# Compiling the PDF

You can recompile the PDF by running `md2pdf.sh`. You need to have `pandoc`
installed.

## Acknowledgment and License

These notes are based on Stewart Weiss's set of notes for his [CSci 340:
Operating Systems](http://www.compsci.hunter.cuny.edu/~sweiss/course_materials/csci340/csci340_spr21.php) course.

Unless noted otherwise all content is released under a [Creative Commons
Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
